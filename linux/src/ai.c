/*
Duel Commander, version 0.2.1.1

Copyright (C) 2009 Tommi Helander
tommi_helander@users.sourceforge.net

This file is part of Duel Commander.

Duel Commander is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Duel Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Duel Commander.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "character.h"

int ai(struct Character *own, struct Character *opponent)
{

	int min = 0;
	int max = 1;
	int needed_ap = 2;
	int accuracy = 3;

	float attack1_real_accuracy = (float)own->attack1[accuracy] * own->accuracy_multiplier;
	float attack2_real_accuracy = (float)own->attack2[accuracy] * own->accuracy_multiplier;
	float attack1_average = ((float)own->attack1[min] + (float)own->attack1[max]) / 2;

	int attack1 = 1;
	int attack2 = 2;
	//int defence1 = 3;
	int defence2 = 4;
	int end_turn = 5;

	float HP_ratio = (float)own->hp / (float)opponent->hp;

	if (own->ap >= own->attack1[needed_ap]) {
		if (own->ap >= own->attack2[needed_ap]*2) {
			if (HP_ratio >= 1.5) {
				if (opponent->hp > (int)(attack1_average * 3) && attack2_real_accuracy >= 0.5) {
					return attack2;
				}
				else {
					return attack1;
				}
			}
			else if (HP_ratio >= 0.75) {
				if (opponent->hp >= own->attack1[max] && opponent->hp <= own->attack2[max] && attack2_real_accuracy >= 0.5) {
					return attack2;
				}
				else if (attack1_real_accuracy >= 0.5) {
					return attack1;
				}
				else {
					return defence2;
				}
			}
			else if (HP_ratio < 0.75) {
				if (own->hp <= (int)(attack1_average * 3) && attack2_real_accuracy >= 0.3) {
					return attack2;
				}
				else if (attack1_real_accuracy >= 0.4) {
					return attack1;
				}
				else {
					return defence2;
				}
			}
		}
		else if (own->ap > own->attack2[needed_ap]) {
			if (attack1_real_accuracy >= 0.5) {
				return attack1;
			}
			else {
				return defence2;
			}
		}
		else if (own->ap == own->attack2[needed_ap]) {
			if (opponent->hp > own->attack1[max]) {
				return attack2;
			}
			else {
				return attack1;
			}
		}
		else if (own->ap >= own->attack1[needed_ap]) {
			if (HP_ratio >= 1.5 && attack1_real_accuracy >= 0.5) {
				return attack1;
			}
			else {
				return defence2;
			}
		}
	}
	else {
		return end_turn;
	}
	return 0;
}
