/*
Duel Commander, version 0.2.1.1

Copyright (C) 2009 Tommi Helander
tommi_helander@users.sourceforge.net

This file is part of Duel Commander.

Duel Commander is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Duel Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Duel Commander.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "random.h"
#include "character.h"
#include "ai.h"

/* FUNCTION PROTOTYPES */
void print_startup_message(char version[8]);
void initialize_character(struct Character *character);
char mainMenu(char version[8]);
char actionMenu(struct Character *P1, struct Character *P2, int turn);
void human_player_attack(struct Character *attacker, struct Character *opponent);
void human_player_defend(struct Character *defender, struct Character *opponent);
void ai_player_attack(int attack, struct Character *attacker, struct Character *opponent);
void ai_player_defend(int defence, struct Character *defender, struct Character *opponent);

/* MAIN FUNCTION */
int main()
{
	char version[8] = "0.2.1.1";
	char selection;             //stores selection from main menu
	char player2_name[25];      //name of player 2
	char user_has_quit;         //1 when user has quit, else 0
	int turn;                   //1 when player 1's turn, 0 when player 2's turn
	int PLAYER1 = 1;
	int PLAYER2 = 0;
	int player1_ended_turn;     //1 when player1 ends turn, else 0
	int player2_ended_turn;     //1 when player2 (human player) ends turn, else 0
	char action;                //stores value from action menu
	int AI_player_ended_turn;   //1 when AI player ends turn, else 0
	int AI_player_action;       //stores value of AI players action of choice
	struct Character player1;   //create an instance of Character called player1
	struct Character player2;   //create an instance of Character called player2

    system("clear");
	print_startup_message(version);

	/* initialize everything */
    initialize_random();
    initialize_character(&player1);
	initialize_character(&player2);

	/* print main menu */
	system("clear");
	selection = mainMenu(version);

	/* main menu loop  */
	while (selection != '3') {
		if (selection == '1' || selection == '2') {
			system("clear");

			/* initalize player names */
			if (selection == '1') {
				char com_name[25] = "COMPUTER";
				strcpy(player2_name, com_name);
			}
			else if (selection == '2') {
				char hu_name[25] = "PLAYER2";
				strcpy(player2_name, hu_name);
			}
			char player1_name[25] = "PLAYER1";
			strcpy(player1.name, player1_name);
			strcpy(player2.name, player2_name);

			user_has_quit = 0;
			turn = random_boolean(50);

			/* fighting loop */
			while (player1.hp > 0 && player2.hp > 0 && user_has_quit == 0) {
				if (turn == PLAYER1) {
					system("clear");
					printf("%s's turn\n", player1.name);
					sleep(1);
					player1_ended_turn = 0;

					/* players turn loop */
					while ( player1.ap > 0 && player1_ended_turn == 0) {
						system("clear");

						action = actionMenu(&player1, &player2, turn);

						if (action == '1') {
							human_player_attack(&player1, &player2);
						}
						else if (action == '2') {
							human_player_defend(&player1, &player2);
						}

						/* user ends turn */
						else if (action == '3') {
							player1_ended_turn = 1;
						}

						/* user quits */
						else if (action == '4') {
							player1.ap = 0;
							user_has_quit = 1;
						}
						else if (action == '\n') {
							printf("You must enter a number between 1 - 4\n");
							sleep(1);
						}
						else {
							printf("Invalid selection\n");
							sleep(1);
						}

						/* end turn if opponent runs out of hp */
						if (player2.hp <= 0) {
							player1_ended_turn = 1;
						}
					}

					/* restore player1 ap and multipliers */
					player1.ap = 10;
					player1.damage_multiplier = 1;
					player1.accuracy_multiplier = 1;

					/* switch turn */
					turn = PLAYER2;
				}
				else if (turn == PLAYER2) {
					system("clear");
					printf("%s's turn\n\n", player2.name);
					sleep(1);

					if (selection == '1') {
						AI_player_ended_turn = 0;

						/* opponents turn loop */
						while (AI_player_ended_turn == 0 && player1.hp > 0) {

							AI_player_action = ai(&player2, &player1);	// get action from AI-engine

							if (AI_player_action == 1 || AI_player_action == 2) {
								ai_player_attack(AI_player_action, &player2, &player1);
							}
							else if (AI_player_action == 3 || AI_player_action == 4) {
								ai_player_defend(AI_player_action - 2, &player2, &player1);
							}
							else if (AI_player_action == 5) {
								AI_player_ended_turn = 1;
							}
							sleep(1);
						}

						/* restore ap and multipliers */
						player2.ap = 10;
						player2.damage_multiplier = 1;
						player2.accuracy_multiplier = 1;

						/* switch turn */
						turn = PLAYER1;
					}
					else if (selection == '2') {

						player2_ended_turn = 0;

						/* players turn loop */
						while ( player2.ap > 0 && player2_ended_turn == 0) {
							system("clear");

							action = actionMenu(&player1, &player2, turn);

							if (action == '1') {
								human_player_attack(&player2, &player1);
							}
							else if (action == '2') {
								human_player_defend(&player2, &player1);
							}

							/* user ends turn */
							else if (action == '3') {
								player2_ended_turn = 1;
							}

							/* user quits */
							else if (action == '4') {
								player2.ap = 0;
								user_has_quit = 1;
							}
							else if (action == '\n') {
								printf("You must enter a number between 1 - 4\n");
								sleep(1);
							}
							else {
								printf("Invalid selection\n");
								sleep(1);
							}

							/* end turn if opponent runs out of hp */
							if (player1.hp <= 0) {
								player2_ended_turn = 1;
							}
						}

						/* restore player1 ap and multipliers */
						player2.ap = 10;
						player2.damage_multiplier = 1;
						player2.accuracy_multiplier = 1;

						/* switch turn */
						turn = PLAYER1;
					}
				}
			}

			/* announce winner */
			if (user_has_quit == 0) {
				if (player1.hp > player2.hp) {
					printf("%s WINS!\n", player1.name);
				}
				else {
					printf("%s WINS!\n", player2.name);
				}
				sleep(2);
			}
		}
		else if (selection == '\n') {
			printf("You must enter a number between 1 & 2\n");
			sleep(1);
		}
		else {
			printf("Invalid selection\n");
			sleep(1);
		}

		/* resurrect characters */
		player1.hp = 100;
		player2.hp = 100;

		system("clear");

		/* print main menu */
		selection = mainMenu(version);
	}
	return 0;
}

/*FUNCTION TO PRINT STARTUP MESSAGE */
void print_startup_message(char version[8])
{
    printf("DUEL COMMANDER, version %s\n", version);
	printf("Copyright (C) 2009 Tommi Helander\n\n");
	printf("This is free software; see license for copying conditions.\n");
	printf("There is ABSOLUTELY NO WARRANTY; not even for MERCHANTIBILITY or\n");
	printf("FITNESS FOR A PARTICULAR PURPOSE. For details, see license file.\n\n");
	printf("Additional information is available at http:\\duelcommander.sourceforge.net\n\n");
	sleep(2);
	printf("Press ENTER to continue...");
	getchar();

	return;
}

/* FUNCTION TO PRINT MAIN MENU */
char mainMenu(char version[8])
{
	char choice;
	char choice_str[50];

	printf("Duel Commander %s\n\n", version);
	printf("-----------------\n");
	printf("| 1) 1 PLAYER\t|\n");
	printf("| 2) 2 PLAYERS\t|\n");
	printf("| 3) QUIT\t|\n");
	printf("-----------------\n");
	printf("Selection: ");
	fgets(choice_str, 50, stdin);
	choice = choice_str[0];

	return choice;
}

/* FUNCTION TO PRINT ACTION MENU AND RETURN CHOICE */
char actionMenu(struct Character *P1, struct Character *P2, int turn)
{
	char choice;
	char choice_str[50];

	printf("Status:\n\n");
	if (turn == 1) {
		printf(">%s:\tHP%d\tAP%d\n", P1->name, P1->hp, P1->ap);
		printf(" %s:\tHP%d\tAP%d\n\n", P2->name, P2->hp, P2->ap);
	}
	else if (turn == 0) {
		printf(" %s:\tHP%d\tAP%d\n", P1->name, P1->hp, P1->ap);
		printf(">%s:\tHP%d\tAP%d\n\n", P2->name, P2->hp, P2->ap);
	}
	printf("Actions:\n");
	printf("---------------------------------------------------------\n");
	printf("| 1) Attack\t2) Defend\t3) End Turn\t4) Quit\t|\n");
	printf("---------------------------------------------------------\n");
	printf("Action: ");
	fgets(choice_str, 50, stdin);
	choice = choice_str[0];

	return choice;
}

/* FUNCTION TO INITIALIZE CHARACTERS */
void initialize_character(struct Character *character)
{
	int min = 0;
	int max = 1;
	int needed_ap = 2;
	int accuracy = 3;

	character->hp = 100;
	character->ap = 10;
	character->attack1[min] = 5;
	character->attack1[max] = 15;
	character->attack1[accuracy] = 75;
	character->attack1[needed_ap] = 3;
	character->attack2[min] = 10;
	character->attack2[max] = 25;
	character->attack2[accuracy] = 50;
	character->attack2[needed_ap] = 5;
	character->defence1[min] = 15;
	character->defence1[max] = 30;
	character->defence1[needed_ap] = 3;
	character->defence2[min] = 15;
	character->defence2[max] = 25;
	character->defence2[needed_ap] = 3;
	character->damage_multiplier = 1;
	character->accuracy_multiplier = 1;

	return;
}

/* HUMAN PLAYERS' ATTACK FUNCTION */
void human_player_attack(struct Character *attacker, struct Character *opponent)
{
	int min = 0;
	int max = 1;
	int needed_ap = 2;
	int accuracy = 3;
	int damage;
	char attack_of_choice;
	char attack_of_choice_str[50];

	do {
		system("clear");

		/* attack menu */
		printf("\t-----------------------------------------------------------------\n");
		printf("1)\t| Attack 1\t| Low damage, high accuracy\t\t\t|\n");
		printf("\t----------------|-----------------------------------------------|\n");
		printf("\t\t\t| DMG: %d - %d HP, ACC: %d %%, AP needed: %d\t|\n", (int)(attacker->attack1[min] * attacker->damage_multiplier), (int)(attacker->attack1[max] * attacker->damage_multiplier), (int)(attacker->attack1[accuracy] * attacker->accuracy_multiplier), attacker->attack1[needed_ap]);
		printf("\t----------------|-----------------------------------------------|\n");
		printf("2)\t| Attack 2\t| High damage, low accuracy\t\t\t|\n");
		printf("\t----------------|-----------------------------------------------|\n");
		printf("\t\t\t| DMG: %d - %d HP, ACC: %d %%, AP needed: %d\t|\n", (int)(attacker->attack2[min] * attacker->damage_multiplier), (int)(attacker->attack2[max] * attacker->damage_multiplier), (int)(attacker->attack2[accuracy] * attacker->accuracy_multiplier), attacker->attack2[needed_ap]);
		printf("\t----------------|------------------------------------------------\n");
		printf("3)\t| Back\t\t|\n");
		printf("\t-----------------\n\n");
		printf("Selection: ");
		fgets(attack_of_choice_str, 50, stdin);
		attack_of_choice = attack_of_choice_str[0];

		/* attack 1 */
		if (attack_of_choice == '1') {
			if (attacker->ap >= attacker->attack1[needed_ap]) {
				int hit_or_not = random_boolean(attacker->attack1[accuracy] * attacker->accuracy_multiplier);
				if (hit_or_not == 1) {
					damage = random_inRange(attacker->attack1[min], attacker->attack1[max]) * attacker->damage_multiplier;
					opponent->hp -= damage;
					printf("%s hits %s for %d HP\n\n", attacker->name, opponent->name, damage);
				}
				else {
					printf("%s MISSED!\n\n", attacker->name);
				}
				attacker->ap -= attacker->attack1[needed_ap];
			}
			else {
				printf("Not enough AP!\n");
			}
			attack_of_choice = '3';
			sleep(1);
		}

		/* attack 2 */
		else if (attack_of_choice == '2') {
			if (attacker->ap >= attacker->attack2[needed_ap]) {
				int hit_or_not = random_boolean(attacker->attack2[accuracy] * attacker->accuracy_multiplier);
				if (hit_or_not == 1) {
					damage = random_inRange(attacker->attack2[min], attacker->attack2[max]) * attacker->damage_multiplier;
					opponent->hp -= damage;
					printf("%s hit %s for %d HP\n\n", attacker->name, opponent->name, damage);
				}
				else {
					printf("%s MISSED!\n\n", attacker->name);
				}
				attacker->ap -= attacker->attack2[needed_ap];
			}
			else {
				printf("Not enough AP!\n");
			}
			attack_of_choice = '3';
			sleep(1);
		}
		else if (attack_of_choice == '3') {
			break;
		}
		else if (attack_of_choice == '\n') {
			printf("You must enter a number between 1 & 2\n");
			sleep(1);
		}
		else {
			printf("Invalid selection\n");
			sleep(1);
		}
	} while (attack_of_choice != '3');
	return;
}

/*HUMAN PLAYERS' DEFENCE FUNCTION */
void human_player_defend(struct Character *defender, struct Character *opponent)
{
	int min = 0;
	int max = 1;
	int needed_ap = 2;
    char defence_to_use;
	char defence_to_use_str[50];

	do {
		system("clear");
		//printf("DEFENCE MOVE\tTYPE\tMIN%%\tMAX%%\tAP\n\n");
		printf("\t---------------------------------------------------------\n");
		printf("1)\t| Defence 1\t| Decreases opponent's damage power\t|\n");
		printf("\t----------------|---------------------------------------|\n");
		printf("\t\t\t| Effect: %d - %d %%\tAP needed: %d\t|\n", defender->defence1[min], defender->defence1[max], defender->defence1[needed_ap]);
		printf("\t----------------|---------------------------------------|\n");
		printf("2)\t| Defence 2\t| Decreases opponent's accuracy\t\t|\n");
		printf("\t----------------|---------------------------------------|\n");
		printf("\t\t\t| Effect: %d - %d %%\tAP needed: %d\t|\n", defender->defence2[min], defender->defence2[max], defender->defence2[needed_ap]);
		printf("\t----------------|----------------------------------------\n");
		printf("3)\t| Back\t\t|\n");
		printf("\t-----------------\n\n");
		printf("Selection: ");
		fgets(defence_to_use_str, 50, stdin);
		defence_to_use = defence_to_use_str[0];

		/* defence 1 */
		if (defence_to_use == '1') {
			if (defender->ap >= defender->defence1[needed_ap]) {
				int decrease_percentage = random_inRange(defender->defence1[min], defender->defence1[max]);
				float multiplier = (100 - (float)decrease_percentage) / 100;
				opponent->damage_multiplier *= multiplier;
				printf("%s's attack power decreased by %d percent\n", opponent->name, decrease_percentage);
				defender->ap -= defender->defence1[needed_ap];
			}
			else {
				printf("Not enough AP\n");
			}
			defence_to_use = '3';
		}

		/* defence 2 */
		else if (defence_to_use == '2') {
			if (defender->ap >= defender->defence2[needed_ap]) {
				int decrease_percentage = random_inRange(defender->defence2[min], defender->defence2[max]);
				float multiplier = (100 - (float)decrease_percentage) / 100;
				opponent->accuracy_multiplier *= multiplier;
				printf("%s's accuracy decreased by %d percent\n", opponent->name, decrease_percentage);
				defender->ap -= defender->defence2[needed_ap];
			}
			else {
				printf("Not enough AP\n");
			}
			defence_to_use = '3';
		}
		else if (defence_to_use == '3') {
			break;
		}
		else if (defence_to_use == '\n') {
			printf("You must enter a number between 1 & 2\n");
		}
		else {
			printf("Invalid input\n");
		}
		sleep(1);
	} while (defence_to_use != '3');
	return;
}

void ai_player_attack(int attack, struct Character *attacker, struct Character *opponent)
{
    int min = 0;
	int max = 1;
	int needed_ap = 2;
	int accuracy = 3;
    int AI_player_hit;
    int damage;

    if (attack == 1) {
        printf("%s is using Attack 1\n", attacker->name);
        sleep(1);
        AI_player_hit = random_boolean(attacker->attack1[accuracy] * attacker->accuracy_multiplier);
        if (AI_player_hit == 1) {
            damage = random_inRange(attacker->attack1[min], attacker->attack1[max]) * attacker->damage_multiplier;
            opponent->hp -= damage;
            printf("%s hit %s for %d HP (left: %d)\n\n", attacker->name, opponent->name, damage, opponent->hp);
        }
        else {
            printf("%s MISSED!\n\n", attacker->name);
        }
        attacker->ap -= attacker->attack1[needed_ap];
    }
    else if (attack == 2) {
        printf("%s is using Attack 2\n", attacker->name);
        sleep(1);
        int AI_player_hit = random_boolean(attacker->attack2[accuracy] * attacker->accuracy_multiplier);
        if (AI_player_hit == 1) {
            damage = random_inRange(attacker->attack2[min], attacker->attack2[max]) * attacker->damage_multiplier;
            opponent->hp -= damage;
            printf("%s hit %s for %d HP (left: %d)\n\n", attacker->name, opponent->name, damage, opponent->hp);
        }
        else {
            printf("%s MISSED!\n\n", attacker->name);
        }
        attacker->ap -= attacker->attack2[needed_ap];
    }
    return;
}

void ai_player_defend(int defence, struct Character *defender, struct Character *opponent)
{
    int min = 0;
	int max = 1;
	int needed_ap = 2;
    int decrease_percentage;
    float multiplier;

    if (defence == 1) {
    }
    else if (defence == 2) {
        printf("%s is using Defence 2\n", defender->name);
        sleep(1);
        decrease_percentage = random_inRange(defender->defence2[min], defender->defence2[max]);
        multiplier = (100 - (float)decrease_percentage) / 100;
        opponent->accuracy_multiplier *= multiplier;
        printf("%s's accuracy is decreased by %d percent\n", opponent->name, decrease_percentage);
        defender->ap -= defender->defence2[needed_ap];
    }
    return;
}
