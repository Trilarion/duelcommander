/*
Duel Commander, version 0.2.1.1

Copyright (C) 2009 Tommi Helander
tommi_helander@users.sourceforge.net

This file is part of Duel Commander.

Duel Commander is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Duel Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Duel Commander.  If not, see <http://www.gnu.org/licenses/>.
*/

int ai(struct Character *own, struct Character *opponent);
