Duel Commander, version 0.2.1.1

Duel Commander is a turn based command line fighting game for Windows and 
Unix-like systems.The game is completely text based with an easy-to-use 
interface.

This version of Duel Commander is a stable version. Even though it's pretty 
stable it still lacks many features of a fully usable program, and 
therefore it can be recommended only for experimental use and as a preview.

Duel Commander is released under the GNU General Public License. For more 
information see 'License' text file.


HOW TO RUN THE GAME:

Extract the Zip -archive, and run DuelCommander.exe to run the game.