/*
Duel Commander, version 0.2.1.1

Copyright (C) 2009 Tommi Helander
tommi_helander@users.sourceforge.net

This file is part of Duel Commander.

Duel Commander is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Duel Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Duel Commander.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <time.h>

void initialize_random()                //randomize rand() by the system clock
{
	time_t seconds;
	time(&seconds);
	srand((unsigned int) seconds);
	rand();         // call once to make next call truely random
	return;
}

int random_boolean(int possibility)
{
	int unit;
	int x;
	unit = RAND_MAX / 100;         // 1 percent = x units
	x = rand();

	if (x < unit * possibility) {  // if x is within the possibility
		return 1;
	}
	else {
		return 0;
	}
}

int random_inRange(int range_min, int range_max)
{
	int difference;
	int range;
	int unit;
	int x;
	int z;

	difference = range_max - range_min;
	range = difference + 1;               // to be able to return 0 too
	unit = RAND_MAX / range;
	x = rand();

	for (z = 1; z <= range; z++) {
		if (x <= z * unit) {             // test if random number is less or
			break;                      // equal. If it is, then break out of
		}                                // the loop
	}
	return range_min + (z - 1);         // substract 1 from z because we have
}                                        // to be able to return 0 and never
                                         // difference + 1
